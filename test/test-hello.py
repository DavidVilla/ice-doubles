#!/usr/bin/python3
# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase, skip

import Ice
Ice.loadSlice('./src/hello.ice')
from Example import Hello, HelloPrx

from mock import Mock


class HelloI(Hello):
    def write(self, msg, current=None):
        print(msg)


class MockHelloI(Hello):
    write = Mock()


class TestHello(TestCase):
    def setUp(self):
        broker = Ice.initialize()

        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)
        return cast.uncheckedCast(proxy)

    @skip(Ice.intVersion() >= 30700)
    def test_trivial_write(self):
        # given
        servant = MockHelloI()
        hello = self.adapter_add(HelloPrx, servant)

        # when
        hello.write("Hello world!")

        # then
        assert servant.write.called

#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase, skip

import Ice
Ice.loadSlice('./src/factorial.ice')
from Example import Math, MathPrx

import mock
import doublex


class MathI(Math):
    def factorial(self, value, current=None):
        result = 1
        while value:
            result *= value
            value -= 1

        return result


class PyMockMathI(MathI):
    factorial = mock.Mock()
    factorial.return_value = 0


class TestPyMock(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)
        return cast.uncheckedCast(proxy)

    def test_mock(self):
        # given
        servant = PyMockMathI()
        math = self.adapter_add(MathPrx, servant)

        # when
        math.factorial(1)

        # then
        assert servant.factorial.called

    def test_stub(self):
        servant = PyMockMathI()
        servant.factorial.return_value = 2
        math = self.adapter_add(MathPrx, servant)
        # set 'math' SUT collaborator


class TestDoublex(TestCase):
    def setUp(self):
        broker = Ice.initialize()
        self.adapter = broker.createObjectAdapterWithEndpoints('adapter', 'tcp')
        self.adapter.activate()

    def adapter_add(self, cast, servant):
        proxy = self.adapter.addWithUUID(servant)
        return cast.uncheckedCast(proxy)

    @skip(Ice.intVersion() >= 30700)
    def test_stub(self):
        # given
        with doublex.Mimic(doublex.Stub, MathI()) as servant:
            servant.factorial(doublex.ANY_ARG).returns(2)

        math = self.adapter_add(MathPrx, servant)

        # when
        result = math.factorial(4)

        # then
        doublex.assert_that(result, doublex.is_(2))

    def test_stub_method(self):
        # given
        servant = Math()
        servant.factorial = doublex.method_returning(2)

        math = self.adapter_add(MathPrx, servant)

        # when
        result = math.factorial(4)

        # then
        doublex.assert_that(result, doublex.is_(2))

    def test_proxyspy(self):
        # given
        servant = doublex.Mimic(doublex.ProxySpy, MathI())
        math = self.adapter_add(MathPrx, servant)

        # when
        result = math.factorial(4)

        # then
        doublex.assert_that(result, doublex.is_(24))
        doublex.assert_that(servant.factorial, doublex.called())

    @skip(Ice.intVersion() >= 30700)
    def test_spy_directly_from_slice(self):
        # given
        with doublex.Mimic(doublex.Spy, Math) as servant:
            servant.factorial(doublex.ANY_ARG).returns(2)

        math = self.adapter_add(MathPrx, servant)

        # when
        result = math.factorial(4)

        # then
        doublex.assert_that(result, doublex.is_(2))
        doublex.assert_that(servant.factorial, doublex.called())

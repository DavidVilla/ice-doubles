// -*- mode:c++ -*-

module Example {
  interface Math {
    long factorial(int value);
  };

  exception RequestCanceledException {};
};

// -*- mode:c++ -*-

module Example {
  interface Hello {
    void write(string message);
  };

  interface HelloDecorator {
    void writeln(Hello* prx, string message);
  };
};
